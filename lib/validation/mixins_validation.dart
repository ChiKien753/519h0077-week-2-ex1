mixin CommonValidation{
  String? validateEmail(String? value){
    if (!value!.contains('@')){
      return 'Please input valid email address';
    }
    return null;
  }

  String? validateNotNull(String? value){
    if (value!.length == 0){
      return 'Please fill this line';
    }
    return null;
  }

  String? validateYear(String? value){
    if (value!.length == 0){
      return 'Please fill this line';
    }else if (int.parse(value) <= 1850 && int.parse(value) >= 2022){
      return 'Year of birth not realistic';
    }
    return null;
  }
  
  String? validatePassword(String? value){
    if(value!.length < 6){
      return 'Password must be at least 6 letters';
    }
    return null;
  }
}