import 'package:flutter/material.dart';
import 'package:w2_ex1_519h0077/validation/mixins_validation.dart';

class App extends StatelessWidget {
  const App({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Log me in",
      home: Scaffold(
        appBar: AppBar(title: Text('Login Form')),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String emailAddress, firstName, lastName, yearOfBirth, address, password;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              fieldEmailAddress(),
              Container(margin: EdgeInsets.only(top: 20.0)),
              fieldFirstName(),
              Container(margin: EdgeInsets.only(top: 20.0)),
              fieldLastName(),
              Container(margin: EdgeInsets.only(top: 20.0)),
              fieldPassword(),
              Container(margin: EdgeInsets.only(top: 20.0)),
              fieldYearOfBirth(),
              Container(margin: EdgeInsets.only(top: 20.0)),
              fieldAddress(),
              Container(margin: EdgeInsets.only(top: 20.0)),
              loginButton()
            ],
          )
        ),
      )
    );
  }

  Widget fieldEmailAddress(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,       
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: 'Email Address'
      ),
      validator: validateEmail,
      onSaved: (value){
        emailAddress = value as String;
      },
    );
  }

  Widget fieldFirstName(){
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'First Name'
      ),
      validator: validateNotNull,
      onSaved: (value){
        firstName = value as String;
      },
    );
  }

  Widget fieldLastName(){
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Last Name'
      ),
      validator: validateNotNull,
      onSaved: (value){
        lastName = value as String;
      },
    );
  }

  Widget fieldPassword(){
    return TextFormField(
      obscureText: true,      //Password type
      decoration: InputDecoration(
        icon: Icon(Icons.lock),
        labelText: 'Password'
      ),
      validator: validatePassword,
      onSaved: (value){
        password = value as String;
      },
    );
  }

  Widget fieldYearOfBirth(){
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.calendar_today),
        labelText: 'Year Of Birth'
      ),
      validator: validateYear,
      onSaved: (value){
        yearOfBirth = value as String;
      },
    );
  }

  Widget fieldAddress(){
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.cabin),
        labelText: 'Address'
      ),
      validator: validateNotNull,
      onSaved: (value){
        address = value as String;
      },
    );
  }

  Widget loginButton(){
    return ElevatedButton(
      onPressed: (){
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();
          print('$emailAddress + $password');
        }
      }, 
      child: Text('Login')
    );
  }
}